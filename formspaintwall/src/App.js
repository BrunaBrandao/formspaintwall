import { Form } from '@unform/web'
import './App.css'

function App() {
  //criando a função para receber o submit do botão

  const handleSubmit = () => {}
  return (
    <form onSubmit={handleSubmit}>
      <input type="number" name="height" placeholder="Altura" />
      <input type="number" name="width" placeholder="Largura" />
      <input type="number" name="doors" placeholder="" />
      <input type="number" name="windows" placeholder="" />
      <button>Salvar</button>
    </form>
  )
}

export default App
